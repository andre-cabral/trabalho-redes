﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public float velocity = 300f;
	public int playerNumber;
	private Vector3 initialPosition;
	private float dyingY;
	private bool dyingYSetted = false;
	public int playerDeathsNumber = 0;
	private Text playerDeathsText;
	private bool playerDeathsInstanced = false;


	NetworkView viewId;

	void Awake(){
		viewId = GetComponent<NetworkView> ();
		initialPosition = transform.position;
	}

	void Update () {
		if(!playerDeathsInstanced){
			Text text = sortFindGamesWithObject(Tags.playerDeathText)[playerNumber].GetComponent<Text>();
			if(text != null){
				playerDeathsText = text;
			}
		}

		if(!(playerDeathsText.text == "Deaths: " + playerDeathsNumber.ToString())){
			playerDeathsText.text = "Deaths: " + playerDeathsNumber.ToString();
		}

		//define a posicao y na qual o jogador ira morrer pegando um objeto da rede
		if(!dyingYSetted){
			GameObject dyingYObj = GameObject.FindGameObjectWithTag(Tags.dyingY);
			if(dyingYObj != null){
				dyingY = dyingYObj.transform.position.y;
			}
		}

		if (viewId.isMine) {
			if (Input.GetAxis ("Vertical") != 0) {
				rigidbody.AddForce(Vector3.forward * Input.GetAxis ("Vertical") * velocity * Time.deltaTime);
			}
			if (Input.GetAxis ("Horizontal") != 0) {
				rigidbody.AddForce (Vector3.right * Input.GetAxis ("Horizontal") * velocity * Time.deltaTime);
			}
		}

		//verifica se o jogador morreu e executa a funcao de morte caso ele tenha morrido
		BellaLugosiIsDead();
	}

	void BellaLugosiIsDead(){
		if(transform.position.y <= dyingY){
			transform.position = initialPosition;
			rigidbody.velocity = Vector3.zero;
			rigidbody.angularVelocity = Vector3.zero;
			playerDeathsNumber++;
			playerDeathsText.text = "Deaths: " + playerDeathsNumber.ToString();
		}
	}

	public void SetPlayerNumber(int playerNumber){
		this.playerNumber = playerNumber;
	}

	public int GetPlayerNumber(){
		return playerNumber;
	}

	public void SetPlayerDeathsNumber(int playerDeathsNumber){
		this.playerDeathsNumber = playerDeathsNumber;
	}

	public int GetPlayerDeathsNumber(){
		return playerDeathsNumber;
	}

	public void SetPlayerDeathsText(Text text){
		playerDeathsText = text;
	}

	GameObject[] sortFindGamesWithObject(string tag){
		GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
		bool checkArray = true;
		while(checkArray){
			checkArray = false;
			for(int i = 0; i < array.Length - 1; i++){
				if( array[i].name.CompareTo(array[i+1].name) > 0 ){
					checkArray = true;
					GameObject temp = array[i];
					array[i] = array[i+1];
					array[i+1] = temp;
				}
			}
		}
		return array;
	}

}
