﻿using UnityEngine;
using System.Collections;

public class TimerManager : MonoBehaviour {

	public float startingTime = 1000f;
	private float currentTime;
	private TimerSync timerSync;
	private bool startedScript = false;

	// Use this for initialization
	void Awake () {
		currentTime = startingTime;
	}
	
	// Update is called once per frame
	void Update () {
		if(startedScript){
			currentTime -= Time.deltaTime;
			timerSync.time = currentTime;
		}
	}

	public void StartTimerManagerScript(){
		timerSync = GameObject.FindGameObjectWithTag(Tags.timerObject).GetComponent<TimerSync>();
		startedScript = true;
	}


}
