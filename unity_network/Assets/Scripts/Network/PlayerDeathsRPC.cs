﻿using UnityEngine;
using System.Collections;

public class PlayerDeathsRPC : MonoBehaviour {

	public int[] playerDeaths = new int[20];

	[RPC]
	void AddDeathToPlayer(int playerNumber){
		playerDeaths[playerNumber]++;
	}
	
	public void SendPlayerDeathsToServer(int playerNumber){
		networkView.RPC("AddDeathToPlayer",RPCMode.All,(int)playerNumber);
	}
	
	public int GetPlayerDeaths (int playerNumber){
		return playerDeaths[playerNumber];
	}
}
