﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateServerObject : MonoBehaviour {

	public GameObject objeto;
	public GameObject stage;
	public GameObject timerObject;
	//private SpawnPositionRPC spawnRPC;
	private GameObject spawnPoint;
	SpawnPointsManager spawnManager;
	TimerManager timerManager;
	public GameObject playerDeathsUI;

	void Awake(){
		//spawnRPC = gameObject.GetComponent<SpawnPositionRPC>();
		spawnManager = gameObject.GetComponent<SpawnPointsManager>();
		timerManager = gameObject.GetComponent<TimerManager>();
	}

	//evento de quando criamos um servidor 
	//E importante este evento para sincronizar variaveis 
	//de configuracao do servidor ou 
	//criar objetos de servidor
	void OnServerInitialized () {
		//cria estagio
		criaObjeto(stage, stage, 0, false);
		criaObjeto(timerObject, timerObject, 0, false);

		criaObjeto(playerDeathsUI, playerDeathsUI, 0, false);

		spawnManager = gameObject.GetComponent<SpawnPointsManager>();

		spawnManager.StartSpawnPointsScript();
		timerManager.StartTimerManagerScript();
		criaObjetoPlayer(objeto, 0);
	}

	GameObject criaObjeto(GameObject objetoCriar, GameObject objetoPosicao, int grupo, bool randomColor){
		//definicao de um grupo de objetos criados
		//int grupo = 0;
		
		//obter a posicao da onde vai ser criado o objeto
		Vector3 posicao = objetoPosicao.transform.position;
		Quaternion rotacao = objetoPosicao.transform.rotation;
		
		//criar objeto na rede
		//teve que dar cast
		//o cast precisou de parenteses na funcao inteira para o cast saber que
		//deve mudar o resultado total de tudo que esta dentro dos parenteses
		GameObject cloneRede = (GameObject)(Network.Instantiate(objetoCriar, posicao, rotacao, grupo));

		if(randomColor){
			cloneRede.renderer.material.color = new Color32 (
				(byte)Random.Range(0,255),
				(byte)Random.Range(0,255),
				(byte)Random.Range(0,255),
				(byte)Random.Range(0,255)
				);
		}

		return cloneRede;
	}

	GameObject criaObjetoPlayer(GameObject player, int grupo){
		GameObject playerRedeClone;
		int playerNumber = 1000203;

		//pega spawn points
		if(spawnManager.GetFirstNonUsedSpawnPoint(false) != null){
			playerNumber = spawnManager.getPlayerNumber();
			spawnPoint = spawnManager.GetFirstNonUsedSpawnPoint(true);
		}
		//Debug.Log(spawnManager.GetFirstNonUsedSpawnPoint());
		
		//cria objeto
		playerRedeClone = criaObjeto(player, spawnPoint, grupo, false);
		playerRedeClone.GetComponent<ColorSync>().currentColor = spawnPoint.GetComponent<SpawnColorDefinition>().currentColor;
		if(playerNumber != 1000203){
			playerRedeClone.GetComponent<PlayerController>().SetPlayerNumber(playerNumber);
		}

		return player;
	}

	GameObject[] sortFindGamesWithObject(string tag){
		GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
		bool checkArray = true;
		while(checkArray){
			checkArray = false;
			for(int i = 0; i < array.Length - 1; i++){
				if( array[i].name.CompareTo(array[i+1].name) > 0 ){
					checkArray = true;
					GameObject temp = array[i];
					array[i] = array[i+1];
					array[i+1] = temp;
				}
			}
		}
		return array;
	}
}
