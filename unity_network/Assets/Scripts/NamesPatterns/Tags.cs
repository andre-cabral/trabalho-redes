﻿using UnityEngine;
using System.Collections;

public class Tags : MonoBehaviour {

	public static string dyingY = "DyingY";
	public static string player = "Player";
	public static string playerDeathText = "PlayerDeathText";
	public static string scoreText = "ScoreText";
	public static string spawnPoint = "SpawnPoint";
	public static string timerNumberText = "TimerNumberText";
	public static string timerObject = "TimerObject";

}
