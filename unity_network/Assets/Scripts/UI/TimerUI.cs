﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimerUI : MonoBehaviour {

	private bool componentTimerStarted = false;
	private Text text;
	TimerSync timerSync;

	// Use this for initialization
	void Awake () {
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		if(!componentTimerStarted){
			if(GameObject.FindGameObjectsWithTag(Tags.timerObject).Length > 0){
				timerSync = GameObject.FindGameObjectWithTag(Tags.timerObject).GetComponent<TimerSync>();
				componentTimerStarted = true;
			}
		}else{
			text.text = Mathf.RoundToInt(timerSync.time).ToString();
		}
	}
}
