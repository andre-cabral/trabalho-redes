﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CreateClientObject : MonoBehaviour {

	//NAO USAR ESSE OBJETO SE ELE FOR UM PREFAB
	//deve-se usar a instancia do instantiate feita com ele (mais abaixo, chamada cloneRede
	//pois o cloneRede acessa o clone em si, que esta na cena, nao um prefab, que pode ter as
	//alteracoes destruidas em mudancas de cena, por exemplo.
	public GameObject objeto;
	private SpawnPositionRPC spawnRPC;
	private Vector3 spawnPosition;
	private Quaternion spawnRotation;
	private bool objetoInstanciado = false;
	private bool networkConnected = false;

	void Awake(){
		spawnRPC = gameObject.GetComponent<SpawnPositionRPC>();
	}


	void OnConnectedToServer(){
		networkConnected = true;
	}

	GameObject criaObjeto(GameObject objetoCriar, Vector3 objetoPosicao, int grupo, bool randomColor){

		//criar objeto na rede
		//teve que dar cast
		//o cast precisou de parenteses na funcao inteira para o cast saber que
		//deve mudar o resultado total de tudo que esta dentro dos parenteses
		GameObject cloneRede = (GameObject)(Network.Instantiate(objetoCriar, objetoPosicao, spawnRotation, grupo));
		
		if(randomColor){
			cloneRede.renderer.material.color = new Color32 (
				(byte)Random.Range(0,255),
				(byte)Random.Range(0,255),
				(byte)Random.Range(0,255),
				(byte)Random.Range(0,255)
				);
		}
		
		return cloneRede;
	}

	GameObject criaObjetoPlayer(GameObject player, int grupo){
		GameObject playerRedeClone;

		spawnPosition = spawnRPC.GetSpawnPosition();
		spawnRotation = spawnRPC.GetSpawnRotation();
		int playerNumber = spawnRPC.GetPlayerNumber();
		
		//cria objeto
		playerRedeClone = criaObjeto(player, spawnPosition, grupo, false);
		playerRedeClone.GetComponent<ColorSync>().currentColor = gameObject.GetComponent<SpawnColorRPC>().currentColor;
		if(playerNumber != 1000203){
			playerRedeClone.GetComponent<PlayerController>().SetPlayerNumber(playerNumber);
		}

		return player;
	}

	void Update(){

		if(!objetoInstanciado && networkConnected){
			//spawnPoints = GameObject.FindGameObjectsWithTag(Tags.spawnPoint);
			//Debug.Log(spawnPosition != null);
			if(spawnPosition != null){
				criaObjetoPlayer(objeto, 0);
				objetoInstanciado = true;
			}
		}

	}

	GameObject[] sortFindGamesWithObject(string tag){
		GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
		bool checkArray = true;
		while(checkArray){
			checkArray = false;
			for(int i = 0; i < array.Length - 1; i++){
				if( array[i].name.CompareTo(array[i+1].name) > 0 ){
					checkArray = true;
					GameObject temp = array[i];
					array[i] = array[i+1];
					array[i+1] = temp;
				}
			}
		}
		return array;
	}
}
