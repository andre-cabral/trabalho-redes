﻿using UnityEngine;
using System.Collections;

public class PositionSync : MonoBehaviour {

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		
		//pra sincronizar precisa ter essa variavel local
		//daria problema se usasse direto o currentColor
		//provavelmente porque pegaria um public vindo de algum lugar q pode nem existir no destino
	

		Vector3 positionNew = new Vector3();
		Quaternion rotationNew = new Quaternion();

		Vector3 velocity = new Vector3();
		Vector3 angularVelocity = new Vector3();
		
		
		
		if (stream.isWriting) {

			positionNew = transform.position;
			rotationNew = transform.rotation;

			velocity = rigidbody.velocity;
			angularVelocity = rigidbody.angularVelocity;

			stream.Serialize(ref positionNew);
			stream.Serialize(ref rotationNew);

			stream.Serialize(ref velocity);
			stream.Serialize(ref angularVelocity);
			
			
		} else {
			//usar a mesma ordem do anterior, para nao dar problema

			stream.Serialize(ref positionNew);
			stream.Serialize(ref rotationNew);

			stream.Serialize(ref velocity);
			stream.Serialize(ref angularVelocity);

			transform.position = positionNew;
			transform.rotation = rotationNew;

			rigidbody.velocity = velocity;
			rigidbody.angularVelocity = angularVelocity;
		}
	}
}
