﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameEndManager : MonoBehaviour {

	private TimerSync timerSync;
	public GameObject gameEndUI;
	private bool gameEnded = false;

	// Use this for initialization
	void Awake () {
		timerSync = GetComponent<TimerSync>();
	}
	
	// Update is called once per frame
	void Update () {
		if(timerSync.time <= 0 && !gameEnded){
			GameEnd();
		}
	}

	private void GameEnd(){
		gameEnded = true;

		GameObject gameEndUIInstance = (GameObject) (Network.Instantiate(gameEndUI, gameEndUI.transform.position, gameEndUI.transform.rotation, 0));

		GameObject.FindGameObjectWithTag(Tags.scoreText).GetComponent<Text>().text = FinalScoreText();

		Time.timeScale = 0f;
	}

	private string FinalScoreText(){
		string finalScoreText = "";

		GameObject[] players = GameObject.FindGameObjectsWithTag(Tags.player);

		foreach(GameObject obj in players){
			finalScoreText += "Player ";
			finalScoreText += obj.GetComponent<PlayerController>().GetPlayerNumber();
			finalScoreText += " - ";
			finalScoreText += obj.GetComponent<PlayerController>().GetPlayerDeathsNumber();
			finalScoreText += " deaths\n";
		}

		return finalScoreText;
	}
}
