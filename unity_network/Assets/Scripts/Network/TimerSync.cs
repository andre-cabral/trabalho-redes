﻿using UnityEngine;
using System.Collections;

public class TimerSync : MonoBehaviour {

	//esse script deve ser adicionado em um network view como COMPONENTE, nao como script
	//NAO EH PRA PASSAR O SCRIPT, E SIM O COMPONENTE DO OBJETO
	
	public float time = 10000f;
	
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		
		//pra sincronizar precisa ter essa variavel local
		//daria problema se usasse direto o currentColor
		//provavelmente porque pegaria um public vindo de algum lugar q pode nem existir no destino
		float newTime = 0f;
		
		
		
		if (stream.isWriting) {
			
			newTime = time;
			
			stream.Serialize(ref newTime);
			
			
		} else {
			//usar a mesma ordem do anterior, para nao dar problema
			stream.Serialize(ref newTime);
			
			time = newTime;
		}
	}
}
