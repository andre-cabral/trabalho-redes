﻿using UnityEngine;
using System.Collections;

public class AutoMovementTest : MonoBehaviour {

	public float velocity = 10f;

	void Update () {
		transform.Translate (Vector3.forward * velocity * Time.deltaTime);
	}

}
