﻿using UnityEngine;
using System.Collections;

public class ColorSync : MonoBehaviour {

		//esse script deve ser adicionado em um network view como COMPONENTE, nao como script
		//NAO EH PRA PASSAR O SCRIPT, E SIM O COMPONENTE DO OBJETO

		public Color currentColor;

		void Start(){
			renderer.material.color = currentColor;
		}

		void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {

			//pra sincronizar precisa ter essa variavel local
			//daria problema se usasse direto o currentColor
			//provavelmente porque pegaria um public vindo de algum lugar q pode nem existir no destino
			float a = 0f;
			float r = 0f;
			float g = 0f;
			float b = 0f;

			

			if (stream.isWriting) {
				
				r = currentColor.r;
				g = currentColor.g;
				b = currentColor.b;
				a = currentColor.a;
							
				stream.Serialize(ref r);
				stream.Serialize(ref g);
				stream.Serialize(ref b);
				stream.Serialize(ref a);

				
			} else {
				//usar a mesma ordem do anterior, para nao dar problema
				stream.Serialize(ref r);
				stream.Serialize(ref g);
				stream.Serialize(ref b);
				stream.Serialize(ref a);

				currentColor.r = r;
				currentColor.g = g;
				currentColor.b = b;
				currentColor.a = a;
				renderer.material.color = currentColor;
			}
		}

}
