﻿using UnityEngine;
using System.Collections;

public class DisconnectedFromServer : MonoBehaviour {

	public GameObject disconnectedFromServerText;
	public float secondsToWait = 3f;
	public string sceneToGoOnDisconnect;

	void OnDisconnectedFromServer(NetworkDisconnection info) {
		disconnectedFromServerText.SetActive(true);
		StartCoroutine(WaitToGoToMenu());
	}

	IEnumerator WaitToGoToMenu(){
		yield return new WaitForSeconds(secondsToWait);
		Application.LoadLevel(sceneToGoOnDisconnect);
	}
}
