﻿using UnityEngine;
using System.Collections;

public class SpawnColorRPC : MonoBehaviour {
	public Color currentColor = new Color();

	[RPC]
	void SetColor(float r, float g, float b,float a){
		currentColor = new Color(r,g,b,a);
	}
	
	public void SendColorToClient(NetworkPlayer player, Color currentColor){
		networkView.RPC("SetColor",player,(float)currentColor.r,(float)currentColor.g,(float)currentColor.b,(float)currentColor.a);
	}
}
