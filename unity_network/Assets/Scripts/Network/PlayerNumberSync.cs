﻿using UnityEngine;
using System.Collections;

public class PlayerNumberSync : MonoBehaviour {

	private PlayerController playerController;

	void Awake(){
		playerController = gameObject.GetComponent<PlayerController>();
	}
	
		
	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		
		//pra sincronizar precisa ter essa variavel local
		//daria problema se usasse direto o currentColor
		//provavelmente porque pegaria um public vindo de algum lugar q pode nem existir no destino
		int playerNumber = 1000203;		
		
		
		if (stream.isWriting) {
			
			playerNumber = playerController.GetPlayerNumber();
			
			stream.Serialize(ref playerNumber);
			
			
		} else {
			//usar a mesma ordem do anterior, para nao dar problema
			stream.Serialize(ref playerNumber);

			playerController.SetPlayerNumber(playerNumber);
		}
	}
}
