﻿using UnityEngine;
using System.Collections;

public class ImpClient : MonoBehaviour {

	public int serverPort = 666;
	public string serverIP = "127.0.0.1";

	// Use this for initialization
	void Start () {
		//inicializamos a conexao com o servidor
		Network.Connect (serverIP, serverPort);
	}
}
