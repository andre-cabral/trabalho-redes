﻿using UnityEngine;
using System.Collections;

public class ImpServer : MonoBehaviour {

	//numero maximo permitido de conexoes para este servidor
	public int numeroConexoes = 1;
	public int portaServidor = 666;

	// Use this for initialization
	void Start () {
		//teste para verificar se a aplicacao esta rodando dentro de uma rede NAT
		bool usarNat = !Network.HavePublicAddress ();
		Network.InitializeServer(numeroConexoes, portaServidor, usarNat);
	}
}
