﻿using UnityEngine;
using System.Collections;

public class SpawnPositionRPC : MonoBehaviour {

	private Vector3 spawnPosition;
	private Quaternion spawnRotation;
	private int playerNumber;
	private float dyingY;

	[RPC]
	void SetSpawnPosition(Vector3 spawnPosition, Quaternion spawnRotation, int playerNumber, float dyingY){
		this.spawnPosition = spawnPosition;
		this.spawnRotation = spawnRotation;
		this.playerNumber = playerNumber;
		this.dyingY = dyingY;
	}

	public void SendSpawnPositionToClient(NetworkPlayer player, Vector3 spawnPosition, Quaternion spawnRotation, int playerNumber, float dyingY){
		networkView.RPC("SetSpawnPosition",player,(Vector3)spawnPosition,(Quaternion)spawnRotation, (int)playerNumber, (float)dyingY);
	}

	public Vector3 GetSpawnPosition (){
		return spawnPosition;
	}

	public Quaternion GetSpawnRotation(){
		return spawnRotation;
	}

	public int GetPlayerNumber(){
		return playerNumber;
	}

	public float GetDyingY(){
		return dyingY;
	}

}
