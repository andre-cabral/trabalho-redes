﻿using UnityEngine;
using System.Collections;

public class SpawnPointsManager : MonoBehaviour {

	private GameObject[] spawnPoints;
	private bool[] spawnPointsInUse;
	private SpawnPositionRPC spawnRPC;
	private SpawnColorRPC spawnColorRPC;
	private float dyingY;

	void Awake(){
		spawnRPC = gameObject.GetComponent<SpawnPositionRPC>();
		spawnColorRPC = gameObject.GetComponent<SpawnColorRPC>();
	}

	public void StartSpawnPointsScript(){
		dyingY = GameObject.FindGameObjectWithTag(Tags.dyingY).transform.position.y;
		spawnPoints = sortFindGamesWithObject(Tags.spawnPoint);
		spawnPointsInUse = new bool[spawnPoints.Length];
		for(int i=0; i > spawnPoints.Length; i++){
			spawnPointsInUse[i] = false;
		}
	}

	void OnPlayerConnected(NetworkPlayer player) {
		//spawnPoints = sortFindGamesWithObject(Tags.spawnPoint);
		if(GetFirstNonUsedSpawnPoint(false) != null){
			int playerNumber = getPlayerNumber();
			GameObject obj = GetFirstNonUsedSpawnPoint(true);

			Vector3 spawnPosition = new Vector3(obj.transform.position.x,obj.transform.position.y,obj.transform.position.z);
			Quaternion spawnRotation = new Quaternion(obj.transform.rotation.x,obj.transform.rotation.y,obj.transform.rotation.z,obj.transform.rotation.w);
			spawnRPC.SendSpawnPositionToClient(player, spawnPosition, spawnRotation, playerNumber, dyingY);
			spawnColorRPC.SendColorToClient(player, obj.GetComponent<SpawnColorDefinition>().currentColor);
			//networkView.RPC("SetSpawnPosition",player,(Vector3)spawnPosition,(Quaternion)spawnRotation);
		}
	}

	void OnPlayerDisconnected(NetworkPlayer player) {
		GameObject[] playersSpheres = GameObject.FindGameObjectsWithTag(Tags.player);

		foreach(GameObject obj in playersSpheres){
			if (obj.networkView.owner == player){
				spawnPointsInUse[obj.GetComponent<PlayerController>().GetPlayerNumber()] = false;
				Debug.Log("Player " + obj.GetComponent<PlayerController>().GetPlayerNumber() + " disconnected...");
			}
		}
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects(player);
	}

	//deve ser usado ANTES do GetFirstNonUsedSpawnPoint(true);
	public int getPlayerNumber(){
		for(int i=0; i < spawnPoints.Length; i++){
			if(!spawnPointsInUse[i]){			
				return i;
			}
		}
		return 1000203;
	}

	public GameObject GetFirstNonUsedSpawnPoint(bool usedTheSpawnPoint){

		for(int i=0; i < spawnPoints.Length; i++){
			if(!spawnPointsInUse[i]){
				if(usedTheSpawnPoint){
					spawnPointsInUse[i] = true;
				}
				return spawnPoints[i];
			}
		}
		return null;
	}

	GameObject[] sortFindGamesWithObject(string tag){
		GameObject[] array = GameObject.FindGameObjectsWithTag(tag);
		bool checkArray = true;
		while(checkArray){
			checkArray = false;
			for(int i = 0; i < array.Length - 1; i++){
				if( array[i].name.CompareTo(array[i+1].name) > 0 ){
					checkArray = true;
					GameObject temp = array[i];
					array[i] = array[i+1];
					array[i+1] = temp;
				}
			}
		}
		return array;
	}

}
